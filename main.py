#!/usr/bin/python3.8
import json
import pprint
from modules.baseClass import TestConfig, TestInfo
from settings import config, logger


def get_config(path):
    logger.debug('Read configurations scenario...')
    try:
        with open(path) as f:
            scenario = json.load(f)
    except Exception as ex:
        logger.error('Error read configurations scenario. Exception:')
        logger.error(ex)
        return False
    return scenario


def init_app():
    logger.info('Get tester configurations...')
    logger.debug('Tester configuration:')

    path = './exemple/sample_test.json'
    config['SystemVars'][0]['%%SCRIPT_PATH%%'] = path

    logger.info('Get test scenario...')
    scenario = get_config(path)
    config['SystemVars'][0]['%%SCENARIO%%'] = scenario

    logger.debug('Base Class Initialization...')
    testObj = TestInfo(config['SystemVars'][0])
    
    testObj.run_case()

if __name__ == "__main__":
    init_app()

