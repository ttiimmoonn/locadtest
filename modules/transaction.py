import logging

from modules.baseUA import BaseUA, BaseUAC, BaseUAS

logger = logging.getLogger("tester")


class Transaction():
    def __init__(self, **kwargs):
        ''' Конструктор одной транзакции.

        method - метод генерации csv
        uac - свойства одного uac
        uas - свойства списка uas

        globalConf - глобальные конфигурации для генерации cmd на запуск sipp
        initPort - порты свободные для sipp клиентов
        initNumber - номера свободные для клиентов

        uaCount - общее количество номеров используемых в транзакции

        '''
        self.method = kwargs.get('type', 'MP2MP') 
        
        self.uacConf = kwargs.get('uac', {})
        self.uasConf = kwargs.get('uas', [])
        
        self.uac = list()
        self.uas = list()

        self.globalConf = kwargs.get('globalConf', {})

        self.initPort = kwargs.get('initPort', 5060)
        self.initNumber = kwargs.get('initNumber', 100)

        self.uaCount = 0

        self._createUser()


    def _createUser(self):
        # Создаем UAC для этой транзакции
        userUAC = BaseUAC(**self.uacConf, **self.globalConf, sourceSipPort=self.initPort)
        
        # Генерируем CSV файл
        if self.method == 'MP2P':
            userUAC.generatCsvMP2P(self.initNumber, self.initPort, len(self.uasConf), 
                                        cdpnNumber=self.uacConf.get('cdpnNumber', ''))
        elif self.method == 'MP2MP':
            userUAC.generatCsvMP2MP(self.initNumber, self.initPort, len(self.uasConf), 
                                        cdpnNumber=self.uacConf.get('cdpnNumber', ''))
        
        self.uaCount = userUAC.usrCount

        idUas = 0
        for uas in self.uasConf:
            self.initPort += 1
            idUas += 1
            userUAS = BaseUAS(**uas, **self.globalConf, sourceSipPort=self.initPort, 
                                        csvPath=userUAC.csvPath, repeatCounter=userUAC.repeatCounter)
            userUAS.generatRegFile(idUas)

            self.uas.append(userUAS)
        self.uac.append(userUAC)
        self.showUserCmd()

    def showUserCmd(self):
        for ua in self.uas:
            ua.generatCmdReg()
            ua.generatCmd()
        for ua in self.uac:
            ua.generatCmdReg()
            ua.generatCmd()