import subprocess
import os
import logging
import random
import csv
from pathlib import PurePath


logger = logging.getLogger("tester")

class BaseUA:
    def __init__(self, typeUA, sourceIp, sourceSipPort, destinationIp, destinationPort, domain, xmlPath, sippPath, mediaPath, tmpPath, regModeAuto=True, timeout=4000):
        self.typeUA = typeUA

        self.domain = domain
        self.sourceIp = sourceIp
        self.sourceSipPort = sourceSipPort
        self.destinationIp = destinationIp
        self.destinationPort = destinationPort


        self.sippPath = sippPath
        self.xmlPath = xmlPath
        self.regXmlPath = str(PurePath(os.getcwd() + '/xml/reg_user.xml'))
        self.csvPath = None
        self.mediaPath = mediaPath
        self.extraOptions = []
        self.tmpPath = tmpPath


        self.timeout = timeout
        self.processSIPp = subprocess.Popen

        self.regModeAuto = regModeAuto

    def __str__(self):
        resTest = '\n\tUser stract:'
        resTest += '\n\t\t├ User type: {}'.format(self.typeUA)
        resTest += '\n\t\t├ Source IP: {}'.format(self.sourceSipPort)
        resTest += '\n\t\t├ Destination: {}:{}'.format(self.destinationIp, self.destinationPort)
        resTest += '\n\t\t├ Xml path: {}'.format(self.xmlPath)
        resTest += '\n\t\t├ Csv path: {}'.format(self.csvPath)
        resTest += '\n\t\t├ timeout: {}'.format(self.timeout)
        return(resTest)

    def _generatCmd(self, xml):
        ''' Создает команду для запуска SIPp для указанного xml.'''
        cmd = []

        cmd.extend([
            self.sippPath,
            '{}:{}'.format(self.destinationIp, self.destinationPort),
            '-sf',
            xml, 
            '-i',
            self.sourceIp,
            '-p',
            str(self.sourceSipPort),
            '-inf',
            self.csvPath
            ])
        if self.mediaPath:
            cmd.extend(['-key media_path', self.mediaPath])
        cmd.extend(self.extraOptions)
        logger.debug('Cmd string: ' + ' '.join(map(str,cmd)))
        return cmd
    
    def generatCmdReg(self):
        ''' Создает команду для запуска регистрации.'''
        return self._generatCmd(self.regXmlPath)
    
    def generatCmd(self):
        ''' Создает команду для запуска клиента.'''
        return self._generatCmd(self.xmlPath)


    @staticmethod
    def writeRegXml(data, tmp_path):
        logger.info('Create xml file...')
        xml_reg_path = str(PurePath(tmp_path + '/regFileID_' + str(random.randint(100000, 999999)) + '.xml'))
        try:
            with open(xml_reg_path, "w") as out_file:
                out_file.write(data)
            logger.debug('File succ create: ' + xml_reg_path)
        except Exception as e:
            logger.error('Error write csv file! Exception:')
            logger.error(e)
        return(xml_reg_path)

    @staticmethod
    def readRegXml(tmp_path):
        logger.info('Read xml file...')
        try:
            with open(tmp_path, "r") as out_file:
                data = out_file.read()
            logger.debug('File succ read: ' + tmp_path)
        except Exception as e:
            logger.error('Error read csv file! Exception:')
            logger.error(e)
        return(data)

    @staticmethod
    def readCsv(csv_path):
        data = []
        logger.info('Read csv file...')
        try:
            with open(csv_path, "r") as out_file:
                csv_data = csv.reader(out_file, delimiter=' ', quotechar=';')
                for row in csv_data:
                    data.append(row)
            logger.debug('File succ read: ' + csv_path)
        except Exception as e:
            logger.error('Error read csv file! Exception:')
            logger.error(e)
        return(data)

    @staticmethod
    def writeCsv(data, tmp_path):
        logger.info('Create csv file...')
        csv_path = str(PurePath(tmp_path + '/' + str(random.randint(100000, 999999)) + '.csv'))
        try:
            with open(csv_path, "w", newline='') as out_file:
                writer = csv.writer(out_file, delimiter=';')
                writer.writerows(data)
            logger.debug('File succ create: ' + csv_path)
        except Exception as e:
            logger.error('Error write csv file! Exception:')
            logger.error(e)
        return(csv_path)    


    def generatRegFile(self, id):
        ''' Генерация файлов регистрации для uas.
        Создает файл для регистрации.

        Keyword arguments:
            id - id в файле csv
        '''
        logger.debug(f'Start gerate register file for user ID: {id}')
        indexList = list(range(6))
        logger.debug('index List: ' + str(indexList))

        new_xmlData = self.readRegXml(self.regXmlPath)

        for user in indexList:
            new_xmlData = new_xmlData.replace(f'[field{user}]', f'[field{int(user) + 7 * id}]')

        regFileName = self.writeRegXml(new_xmlData, self.tmpPath)      
        logger.debug(f'Write new regester file in tmp: {regFileName}')

        self.regXmlPath = regFileName


class BaseUAC(BaseUA):
    def __init__(self, **kwargs):
        BaseUA.__init__(self, 
            'UAC', kwargs.get('sourceIp', ''), 
            kwargs.get('sourceSipPort', ''), 
            kwargs.get('destinationIp', ''), 
            kwargs.get('destinationPort', ''),
            kwargs.get('domain', ''),
            str(PurePath(kwargs.get('xmlPath', '') + '/' + kwargs.get('xmlName', ''))),
            kwargs.get('sippPath', ''),
            kwargs.get('mediaPath', ''),
            kwargs.get('tmpPath', '')
        )

        self.csvPath = ''

        # Максимальное колчество одновременных вызовов
        self.limitCalls = kwargs.get('limitCalls', 10)
        self.extraOptions.extend(['-l', str(self.limitCalls)])

        # Общее количество вызов в этом тесте
        self.repeatCounter = kwargs.get('repeatCounter', 30)
        self.extraOptions.extend(['-m', str(self.repeatCounter)])

        # Скорость звонка
        self.rate = kwargs.get('rate', 3)
        self.extraOptions.extend(['-r', str(self.rate)])

        # Период изменения трафика звонка
        self.ratePeriod = kwargs.get('ratePeriod', '')
        if self.ratePeriod:
            self.extraOptions.extend(['-rp', str(self.ratePeriod)])

        # Значение на которое будет увеличено количество вызовов за время rate
        self.rateIncrease = kwargs.get('rateIncrease', '')
        if self.rateIncrease:
            self.extraOptions.extend(['-rate_increase', str(self.rateIncrease)])

        self.usrCount = 0

        self.uasRegXmlList = []

    def generatCsvMP2MP(self, initNumber, initSipPort, countUserUAS, cdpnNumber='', userPassword='123', expires=3600):
        # Порт для UAC
        portUserUAC = initSipPort

        # Порт для первого UAS
        portUserUAS = initSipPort + 1

        # Первый номер для абонентов
        initialNumber = initNumber
        
        # Создаем легенду для таблицы
        #   Первая строка для указания csv sipp
        res_date_UAC = [['SEQUENTIAL']]
        
        #   Шаблоны легенды
        data_cgpn = ['#cgpn', 'cgpn_dname', 'authentication[username password]', 'cgpn_domain', 'def_local_port', 'expires', 'remote_number']
        data_cdpn = ['cdpn', 'cdpn_dname', 'authentication[username password]', 'cdpn_domain', 'def_local_port', 'expires']
        
        data_cgpn.extend(data_cdpn * countUserUAS)
        res_date_UAC.append(data_cgpn)

        for userUAC in list(range(self.limitCalls)):
            if cdpnNumber:
                cr_cgpn_part = [
                    str(initialNumber), 
                    str(initialNumber), '[authentication username={} password={}]'.format(initialNumber, userPassword), 
                    str(self.domain), str(portUserUAC), 
                    str(expires), str(cdpnNumber)
                ]
            else:
                cr_cgpn_part = [
                    str(initialNumber), 
                    str(initialNumber), '[authentication username={} password={}]'.format(initialNumber, userPassword), 
                    str(self.domain), str(portUserUAC), 
                    str(expires), str(int(initialNumber) + 1)
                ]

            for userUAS in list(range(countUserUAS)):
                cdpn = str(int(initialNumber) + 1 + int(userUAS))
                cr_cdpn_part = [
                    str(cdpn), str(cdpn), 
                    '[authentication username={} password={}]'.format(cdpn, userPassword), 
                    str(self.domain), str(int(portUserUAS) + int(userUAS)), 
                    str(expires)
                ]
                cr_cgpn_part.extend(cr_cdpn_part)

            res_date_UAC.append(cr_cgpn_part)
            initialNumber += int(countUserUAS) + 1		

        self.usrCount = initialNumber - initNumber
        self.csvPath = self.writeCsv(res_date_UAC, self.tmpPath)

    def generatCsvMP2P(self, initNumber, initSipPort, countUserUAS, cdpnNumber='', userPassword='123', expires=3600):
        # Порт для UAC
        portUserUAC = initSipPort

        # Порт для первого UAS
        portUserUAS = initSipPort + 1

        # Первый номер для абонентов
        initialNumber = initNumber
        
        initialCdpnNumber = initNumber + 1

        # Создаем легенду для таблицы
        #   Первая строка для указания csv sipp
        res_date_UAC = [['SEQUENTIAL']]
        
        #   Шаблоны легенды
        data_cgpn = ['#cgpn', 'cgpn_dname', 'authentication[username password]', 'cgpn_domain', 'def_local_port', 'expires', 'remote_number']
        data_cdpn = ['cdpn', 'cdpn_dname', 'authentication[username password]', 'cdpn_domain', 'def_local_port', 'expires']
        
        data_cgpn.extend(data_cdpn * countUserUAS)
        res_date_UAC.append(data_cgpn)

        cr_cdpn_part = []

        cdpn = initialCdpnNumber
        for userUAS in list(range(countUserUAS)):
            cdpn = str(int(cdpn) + int(userUAS))
            cr_cdpn_part.extend([
                str(cdpn), 
                str(cdpn), 
                '[authentication username={} password={}]'.format(cdpn, userPassword), 
                str(self.domain), 
                str(int(portUserUAS) + int(userUAS)), 
                str(expires)
                ])
        initialNumber = int(cdpn) + 1

        for userUAC in list(range(self.limitCalls)):
            if cdpnNumber:
                cr_cgpn_part = [
                    str(initialNumber), str(initialNumber), 
                    '[authentication username={} password={}]'.format(initialNumber, userPassword), 
                    str(self.domain), str(portUserUAC), str(expires), str(cdpnNumber)
                ]
            else:
                cr_cgpn_part = [
                    str(initialNumber), str(initialNumber), 
                    '[authentication username={} password={}]'.format(initialNumber, userPassword), 
                    str(self.domain), str(portUserUAC), 
                    str(expires), str(int(initialCdpnNumber))
                ]

            cr_cgpn_part.extend(cr_cdpn_part)
            res_date_UAC.append(cr_cgpn_part)
            initialNumber += 1			

        self.usrCount = initialNumber - initNumber - 1
        self.csvPath = self.writeCsv(res_date_UAC, self.tmpPath)


    def generatCsvP2MP(self, initNumber, initSipPort, countUserUAS, tmpPath, cdpnNumber='', userPassword='123', expires=3600):
        pass



class BaseUAS(BaseUA):
    def __init__(self, **kwargs):
        BaseUA.__init__(self, 
            'UAS', kwargs.get('sourceIp', ''), 
            kwargs.get('sourceSipPort', ''), 
            kwargs.get('destinationIp', ''), 
            kwargs.get('destinationPort', ''),
            kwargs.get('domain', ''),
            str(PurePath(kwargs.get('xmlPath', '') + '/' + kwargs.get('xmlName', ''))),
            kwargs.get('sippPath', ''),
            kwargs.get('mediaPath', ''),
            kwargs.get('tmpPath', '')
        )


        # Общее количество вызов в этом тесте
        self.repeatCounter = kwargs.get('repeatCounter', 30)
        self.extraOptions.extend(['-m', str(self.repeatCounter)])

        self.csvPath = kwargs.get('csvPath', '')

        self.uasCount = kwargs.get('uasCount', 10)


'''
a = BaseUAC({
            'sourceIp': '192.168.116.142', 
            'sourceSipPort': 5060, 
            'destinationIp': '192.168.116.182', 
            'destinationPort': 5062,
            'xmlPath': './xmlPath',
            'sippPath': './sipp'
    })

a.generatCsv(100, 5060, 10, 'tm.autotests', '/tmp/load_test', type='MP2P')

a.runUA()
'''