import logging
import copy

from modules.transaction import Transaction

logger = logging.getLogger("tester")

class TestCase():
    ''' Тест кейс.'''
    def __init__(self, **kwargs):
        ''' Конструктор тест кейсов.
        name - имя тест кейса
        description - описание тест кейса
        type - типа взоимодействия пользователей

        uacClientCount - количество клиентов sipp для запуска ua
        uaсCount - количество uac
        uasCount - количество uas

        initPort - порт начиная с которого будут заниматься порты для тест кейса
        initNumber - номер с которого будут заниматься номера для тест кейса
        userUas - объекты UAS пользователей
        userList - объекты пользователей для запуска

        usersXML - параметры пользователей
        '''
        self.name = kwargs.get('name', '')
        self.description = kwargs.get('description', '')
        self.type = kwargs.get('type', '')
        self.globalConf = kwargs.get('globalConf', {})

        self.userUasConfigList = list()
        self.userUacConfigList = list()
        self._getConfigUas(kwargs.get('usersXML', []))

        self.initPort = kwargs.get('initPort', 5070)
        self.initNumber = kwargs.get('initNumber', 100)
        self.uasCount = len(self.userUasConfigList)
        self.uacCount = len(self.userUacConfigList)
        self.uaClientCount = len(self.userUasConfigList) + len(self.userUacConfigList)

        self.transactionList = list()
        self._createTransaction()


    def _getConfigUas(self, usersXML):
        for ua in usersXML:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
            if ua.get("typeUA") == 'uac':
                self.userUacConfigList.append(ua)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            elif ua.get("typeUA") == 'uas':
                self.userUasConfigList.append(ua)

    def _createTransaction(self):
        for uacConf in self.userUacConfigList:
            transact = Transaction(uac=uacConf, uas=self.userUasConfigList, globalConf=self.globalConf, 
                                    type=self.type, initPort=self.initPort, initNumber=self.initNumber)
            self.initPort += 1 + self.uasCount
            self.initNumber += 1 + transact.uaCount
            self.transactionList.append(transact)

#    user = BaseUAC(**ua, **self.globalConf, sourceSipPort=self.initPort)
