import logging
import csv
from pathlib import PurePath
import random

from modules.baseUA import BaseUA, BaseUAC, BaseUAS
from modules.testCase import TestCase

logger = logging.getLogger("tester")

class TestConfig:
	''' Класс для работы с глобальными параметрами теста.

	sippPath - путь до sipp
	tmpPath - путь до директории с временными файлами
	scr - путь то файла с параматрами тест кейса
	media_path - путь до директории с медиа файлами
	xml_scr_path - путь до директории с xml сценариями
	exter_ip - ip тестируемого устройства
	exter_port - порт тестируемоего устройства
	ip - локальный ip адресс
	exter_dom - PBX домен тестируемого устройства
	initUserPort - порт с которого начинается отсчет портов для абонентов
	initUserNumber - номера пользователей с которых начинается отсчет 
	'''
	def __init__(self, config):
		self.sippPath = config.get('%%SIPP_PATH%%')
		self.tmpPath = config.get('%%TEMP_PATH%%')
		self.scr = config.get('%%scenario%%')
		self.media_path = config.get('%%MEDIA_PATH%%')
		self.scr_path = config.get('%%SCRIPT_PATH%%')
		self.xml_scr_path = config.get('%%XML_SRC_PATH%%')
		self.exter_ip = config.get('%%EXTER_IP%%')
		self.exter_port = config.get('%%EXTER_PORT%%')
		self.ip = config.get('%%IP%%')
		self.exter_dom = config.get('%%DEV_DOM%%')

		self.initUserPort = config.get('%%INIT_PORT%%')
		self.initUserNumber = config.get('%%INIT_NUMBER%%')

	def return_global_config(self):
		return {
			'sourceIp': self.ip,
			'destinationIp': self.exter_ip,
			'destinationPort': self.exter_port,
			'sippPath': self.sippPath,
			'xmlPath': self.xml_scr_path,
			'mediaPath': self.media_path,
			'domain': self.exter_dom,
			'tmpPath': self.tmpPath
		}


class TestInfo(TestConfig):
	def __init__(self, config):
		''' Базорвый класс для обработки всех параметров переданных для теста.

		freePort - порт с которого начинатся отсчет свободных портов для sipp юзеров
		freeNumber - номер с которого начинается отсчет свободных номер для обзвона

		PreConfSSW/PostConfSSW - конфигурации перед и после выполнения группы тестов
		CaseList - массив из кейсов для запуска
		RegUserList - пользователи, которых нужно переодически автоматически регистрировать
		'''

		TestConfig.__init__(self, config)

		self.PreConfSSW = config['%%SCENARIO%%'].get('PreConfSSW', [])
		self.PostConfSSW = config['%%SCENARIO%%'].get('PostConfSSW', [])

		self.CaseList = self.initTestCase(config)

	def __str__(self):
		resTest = '\nMain TestConfig'
		resTest += '\nSIPP path: {}'.format(self.sippPath)
		resTest += '\nScript path: {}'.format(self.scr_path)
		resTest += '\nXML script path: {}'.format(self.xml_scr_path)
		resTest += '\nExter IP: {}'.format(self.exter_ip)
		resTest += '\nExter Port: {}'.format(self.exter_port)
		resTest += '\nIP: {}'.format(self.ip)
		resTest += '\nExter domain: {}'.format(self.exter_dom)
		resTest += '\n'
		resTest += '\nTestInfo'
		resTest += '\nPreConfSSW:'
		if self.PreConfSSW:
			for conf in self.PreConfSSW:
				resTest += '\n	├ {}'.format(conf)
		else:
			resTest += ' None'

		resTest += '\nPostConfSSW:'
		if self.PostConfSSW:
			for conf in self.PostConfSSW:
				resTest += '\n	├ {}'.format(conf)
		else:
			resTest += ' None'

		resTest += '\nCaseList:'
		if self.CaseList:
			for case in self.CaseList:
				resTest += '\n\tTest name: {}'.format(case.name)
				resTest += '\n\t\t├ Test description: {}'.format(case.description)
				resTest += '\n\t\t├ UAS user list:'
				for uaInCase in case.userUas:
					resTest += 	str(uaInCase)

				resTest += '\n\tUAC user list:'
				for uaInCase in case.userUac:
					resTest += str(uaInCase)
		else:
			resTest += ' None'

		resTest += '\n'
		return resTest
	
	def initTestCase(self, config):
		""" Функция генерирует внутренний массив для запуска тестов:
			return (массив из кейсов):
			[ modules.TestCase, ..., modules.TestCase ] 
		"""
		genCasesList = []
		cases = config['%%SCENARIO%%'].get('CaseList', [])

		logger.debug('Start init test case...')
		# Создадим новый объект тест кейса
		for case in cases: 
			testCase = TestCase(**case, globalConf=self.return_global_config(), 
								initPort=self.initUserPort, initNumber=self.initUserNumber)

			self.initUserPort += testCase.uaClientCount
			self.initUserNumber = self.initUserNumber + testCase.uasCount + testCase.uacCount

			genCasesList.append(testCase)
		return genCasesList

	def initRegUser(self):
		for case in self.CaseList:
			for user in case['userUac']:
				user.generatRegFile(self.tmpPath, int(case['uasCount']))
				for xmlUasReg in user.uasRegXmlList:
					logger.info(f'Path to xml UAS schenema: {xmlUasReg}')
				logger.info(f'Path to xml UAC schenema: {user.regXmlPath}')

	def run_case(self):
		for case in self.CaseList:
			pass
