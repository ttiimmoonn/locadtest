import pathlib
import json
import logging


BASE_DIR = pathlib.Path(__file__).parent

# Путь до конфигураций
config_path = BASE_DIR / 'config' / 'config.json'
# Путь до файлов с логами
log_path = BASE_DIR / 'logs'


def create_logger(path):
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    handler = logging.FileHandler("{}/loadTestError.log".format(path), "w", encoding=None, delay="true")
    handler.setLevel(logging.ERROR)
    formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    handler = logging.FileHandler("{}/loadTestAll.log".format(path), "w")
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)-8s %(levelname)-8s [%(module)s:%(lineno)d] %(message)-8s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


def get_config(path):
    with open(path) as f:
        config = json.load(f)
    return config


logger = create_logger(log_path)
config = get_config(config_path)
