# LOADTEST

## Файлы конфигурации:

Конфигурация CaseList:
```json
        {
            "type": "MP2P",                    тип кейса (MP2P/MP2MP)
            "backMode": true,                            запускать ли кейс в фоновом режиме

    <!------------ Опциональные параметры ------------>                   
            "timeout": 30,                               максимальное время теста (в секундах)
            
            "usersXML": [
                {
                    "typeUA": "uac",                     тип юзеров, которые будут использоваться (uac/uas)
                    "xmlName": "uac_recv_bye.xml",       ссылка на xml скрипт для sipp
                    "limitCalls": 100,                   максимальное колчество одновременных диалогов (sipp -l)
                    "repeatCounter": 10000,              общее количество вызов в этом кейсе (sipp -m)  
                    "ratePeriod": 20,                    период изменения трафика звонка (sipp -rp)
                    "rate": 5,                           максимальное количество вызово в секунду (sipp -r)
                    "rateIncrease": 20,                  значение на которое будет увеличено количество вызовов за время ratePeriod (-rate_increase)
                    "regMode": "Auto"                    автоматически регистрировать и поддерживать регистрацию для абонентов
                },
                {
                    "typeUA": "uac",
                    "xmlName": "uac_recv_bye.xml",
                    "limitCalls": 700,
                    "repeatCounter": 1,
                    "ratePeriod": 20,
                    "rate": 20,
                    "rateIncrease": 20,
                },
                ...
                {
                    "typeUA": "uas",
                    "xmlName": "uas_recv_bye.xml"
                }
                
            ]
        }
        ...
```